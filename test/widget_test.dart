import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {


  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(const MyWidget(title: 'T', message: 'M'));

    // Create the Finders.
    final titleFinder = find.text('T');
    final messageFinder = find.text('M');

    // Use the findsOneWidget matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });



  testWidgets('seeker', (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    final addField = find.byKey(ValueKey("userTextField"));




  });
  testWidgets('seeker1', (WidgetTester tester) async {
    final addField2 = find.byKey(ValueKey("passTextField"));
  });


  testWidgets('seeker2', (WidgetTester tester) async {
    final addField3 = find.byKey(ValueKey("siButt"));
  });

  testWidgets('seeker3', (WidgetTester tester) async {
    final addField3 = find.byKey(ValueKey("regButt"));
  });




}

class MyWidget extends StatelessWidget {
  const MyWidget({
    Key? key,
    required this.title,
    required this.message,
  }) : super(key: key);

  final String title;
  final String message;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                TextField(
                    key: const Key('userTextField'),
                    style: TextStyle(color: Colors.white),
                    ),
                TextField(
                    key: const Key('passTextField'),
                    style: TextStyle(color: Colors.white),
                    obscureText: true,
                    ),
                Text(message),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                        key: Key("siButt"),
                        color: Colors.white,
                        icon: Icon(Icons.login_outlined), onPressed: () {  },),
                    IconButton(
                        key: Key("regButt"),
                        color: Colors.white,
                        icon: Icon(Icons.app_registration_outlined), onPressed: () {  },),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}