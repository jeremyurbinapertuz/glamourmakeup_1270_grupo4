import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getglam/domain/use_case/controllers/chat_controller.dart';
import 'package:getglam/domain/use_case/controllers/connectivity.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:getglam/domain/use_case/controllers/controllerfirestore.dart';
import 'package:getglam/domain/use_case/controllers/controlrealtime.dart';
import 'package:getglam/domain/use_case/controllers/location_controller.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';
import 'package:getglam/ui/app.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:workmanager/workmanager.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager().initialize(
    updatePositionInBackground,
    isInDebugMode: true,
  );
  await Firebase.initializeApp();
  Get.put(Controllerauth());
  Get.put(PerfilController());
  Get.put(ControllerRealtime());
  Get.put(ChatController());
  Get.put(ControllerFirestore());

  // Connectivity Controller
  ConnectivityController connectivityController =
  Get.put(ConnectivityController());
  // Connectivity stream
  Connectivity().onConnectivityChanged.listen((connectivityStatus) {
    connectivityController.connectivity = connectivityStatus;
  });

  //Controlador para la ubicacion
  Get.put(LocationController());

  // Run MyApp
  runApp(const MyApp());
}

signOutDialog(BuildContext context) {
  // set up the buttons
  Widget cancelButton = IconButton(
      key: Key("cSOButton"),
      color: Colors.white,
      onPressed: () {
        Navigator.of(context).pop();
      },
      icon: FaIcon(
        FontAwesomeIcons.solidTimesCircle,
        color: new Color.fromRGBO(220, 53, 69, 1),
      ),);
  Widget continueButton = IconButton(
    key: Key("sOButton"),
    color: Colors.white,
    onPressed: () {
      Navigator.of(context).pop();
      Controllerauth().signOut();
    },
    icon: FaIcon(
      FontAwesomeIcons.checkCircle,
      color: new Color.fromRGBO(40, 167, 69, 1),
    ),);
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Cerrar Sesión"),
    content: Text("Estás seguro de que quieres salir?"),
    actions: [
      cancelButton,
      continueButton,
    ],
  );
  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

void updatePositionInBackground() async {
  Workmanager().executeTask((task, inputData) async {
    LocationController controlubicacion = Get.find();
    controlubicacion.obtenerubicacion();
    return Future.value(true);
  });
}

