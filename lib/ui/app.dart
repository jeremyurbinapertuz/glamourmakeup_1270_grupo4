import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:getglam/ui/routes/app_pages.dart';
import 'package:getglam/ui/pages/login/loginpage.dart';
import 'package:getglam/ui/themes/app_themes.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'GetGlam',
      debugShowCheckedModeBanner: false,
      home: Login(),
      getPages: AppPages.list,
      darkTheme: AppThemes.darkTheme,
      theme: AppThemes.ligthTheme,
      themeMode: ThemeMode.system,
    );
  }
}

