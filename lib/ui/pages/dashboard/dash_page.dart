import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/connectivity.dart';
import 'package:getglam/domain/use_case/controllers/dash_controller.dart';
import 'package:getglam/ui/pages/home/home_page.dart';
import 'package:getglam/ui/pages/location/location.dart';
import 'package:getglam/ui/pages/profile/perfil_page.dart';
import 'package:getglam/ui/pages/chat/chat_page.dart';
import 'package:getglam/ui/pages/buscar/buscar_page.dart';

class DashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    late ConnectivityController connectivityController;
    connectivityController = Get.find<ConnectivityController>();
    return GetBuilder<DashController>(builder: (controller) {
      return Scaffold(
        body: SafeArea(
          child: IndexedStack(
            index: controller.tabIndex,
            children: [
              Obx(() => (connectivityController.connected)
                  ? HomePage()
                  : Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.wifi_off),
                            Text("Sin conexión a internet")
                          ]),
                    )),
              Obx(() => (connectivityController.connected)
                  ? PerfilPage()
                  : Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.wifi_off),
                            Text("Sin conexión a internet")
                          ]),
                    )),
              Obx(() => (connectivityController.connected)
                  ? Chat()
                  : Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.wifi_off),
                            Text("Sin conexión a internet")
                          ]),
                    )),
              Obx(() => (connectivityController.connected)
                  ? BuscarPage()
                  : Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.wifi_off),
                            Text("Sin conexión a internet")
                          ]),
                    )),
              Obx(() => (connectivityController.connected)
                  ? LocationPage()
                  : Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.wifi_off),
                      Text("Sin conexión a internet")
                    ]),
              )),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.amber,
          selectedItemColor: Colors.amber[100],

          onTap: controller.changeTabIndex,

          currentIndex: controller.tabIndex,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: true,
          showUnselectedLabels: false,
          backgroundColor: Colors.purple.shade900,
          //backgroundColor: Colors.white,
          elevation: 0,

          items: [
            _bottomNavigationBarItem(
              icon: Icons.home_outlined,
              label: 'Home',

              //backgroundColor: Colors.purple.shade900,
            ),
            _bottomNavigationBarItem(
              icon: Icons.account_box_outlined,
              label: "Perfil",
              //backgroundColor: Colors.deepPurple.shade900,
            ),
            _bottomNavigationBarItem(
              icon: Icons.markunread_outlined,
              label: "Chat",
              //backgroundColor: Colors.lightBlue.shade900,
            ),
            _bottomNavigationBarItem(
              icon: Icons.catching_pokemon,
              label: "Galeria",
              //backgroundColor: Colors.pink.shade800,
            ),
            _bottomNavigationBarItem(
              icon: Icons.location_on,
              label: "GPS",
              //backgroundColor: Colors.pink.shade800,
            ),
          ],
        ),
      );
    });
  }

  _bottomNavigationBarItem({
    required IconData icon,
    required String label,
    /*required  Color backgroundColor*/
  }) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
      //backgroundColor: backgroundColor,
    );
  }
}
