import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/buscar_controller.dart';
import 'package:getglam/domain/use_case/controllers/chat_controller.dart';
import 'package:getglam/domain/use_case/controllers/dash_controller.dart';
import 'package:getglam/domain/use_case/controllers/home_controller.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';



class DashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashController>(() => DashController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<PerfilController>(() => PerfilController());
    Get.lazyPut<ChatController>(() => ChatController());
    Get.lazyPut<BuscarController>(() => BuscarController());

  }
}
