import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getglam/domain/models/mensajes.dart';
import 'package:getglam/domain/use_case/controllers/chat_controller.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';
import 'package:getglam/domain/use_case/controllers/themecontroller.dart';
import 'package:getglam/ui/pages/chat/wmensajes.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:getglam/ui/widgets/appbar.dart';

class Chat extends StatefulWidget {
  Chat({Key? key}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _mensajeController = TextEditingController();
  ChatController controlchat = Get.find();
  Controllerauth controluser = Get.find();
  PerfilController controlp = Get.find();
  final ThemeController controller = Get.find();


  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) => _scrollHaciaAbajo());

    return Scaffold(
        appBar: CustomAppBar(
          controller: controller,
          controluser: controluser,
          context: context,
        ),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(children: [
              _getListaMensajes(),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Flexible(
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: TextField(
                            key: Key('msgTextField'),
                            keyboardType: TextInputType.text,
                            controller: _mensajeController,
                            onChanged: (text) => setState(() {}),
                            onSubmitted: (input) {
                              _enviarMensaje();
                            },
                            decoration: const InputDecoration(
                                hintText: 'Escribe un mensaje')))),
                IconButton(
                    key: Key('sendMsgButton'),
                    icon: Icon(_puedoEnviarMensaje()
                        ? CupertinoIcons.arrow_right_circle_fill
                        : CupertinoIcons.arrow_right_circle),
                    onPressed: () {
                      _enviarMensaje();
                    })
              ]),
            ])));
  }

  void _enviarMensaje() {
    if (_puedoEnviarMensaje()) {
      final mensaje = Mensaje(
          _mensajeController.text,
          DateTime.now(),
          controluser.userf,
          controlp.nombre,
          controlp.fotoperfil,
          controluser.uid);
      controlchat.guardarMensaje(mensaje);
      _mensajeController.clear();
      setState(() {});
    }
  }

  bool _puedoEnviarMensaje() => _mensajeController.text.length > 0;


  // Widget _getListaMensajes() {
  //   String uid = controluser.getUid();
  //   return GetX<ChatController>(builder: (_mensajeController) {
  //     WidgetsBinding.instance!.addPostFrameCallback((_) => _scrollHaciaAbajo());
  //     return Expanded(
  //         child: FirebaseAnimatedList(
  //       controller: _scrollController,
  //       query: controlchat.getMensajes(),
  //       itemBuilder: (context, snapshot, animation, index) {
  //         final json = snapshot.value as Map<dynamic, dynamic>;
  //         print('Id_unico:${snapshot.key}');
  //         String? key = snapshot.key;
  //         final mensaje = Mensaje.fromJson(json);
  //         return MensajeWidget(mensaje.texto, mensaje.fecha, key!,
  //             mensaje.email, mensaje.name, mensaje.photo, uid);
  //       },
  //     ));
  //   });
  // }

  Widget _getListaMensajes() {
    return Expanded(
        child: FirebaseAnimatedList(
      controller: _scrollController,
      query: controlchat.getMensajes(),
      itemBuilder: (context, snapshot, animation, index) {
        final json = snapshot.value as Map<dynamic, dynamic>;
        print('Id_unico:${snapshot.key}');
        String? key = snapshot.key;
        final mensaje = Mensaje.fromJson(json);
        return MensajeWidget(mensaje.texto, mensaje.fecha,  key!, mensaje.email, mensaje.name, mensaje.photo, mensaje.uid);
      },
    ));
  }

  void _scrollHaciaAbajo() {
    if (_scrollController.hasClients) {
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    }
  }
}
