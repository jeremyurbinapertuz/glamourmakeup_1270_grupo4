import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/camera_controller.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';
import 'package:getglam/domain/use_case/controllers/themecontroller.dart';
import 'package:getglam/ui/pages/camera/camera_page.dart';
import 'package:getglam/ui/pages/profile/edit_profile.dart';
import 'package:getglam/ui/widgets/appbar.dart';
import 'package:getglam/ui/widgets/button_widget.dart';
import 'package:getglam/ui/widgets/numbers_widget.dart';
import 'package:getglam/ui/widgets/profile_widget.dart';

class PerfilPage extends StatefulWidget {
  @override
  _PerfilPageState createState() => _PerfilPageState();
}

late final ThemeController controller = Get.put(ThemeController());

class _PerfilPageState extends State<PerfilPage> {
  PerfilController controlp = Get.find();
  Controllerauth controluser = Get.find();
  TextEditingController controltitulo = TextEditingController();
  TextEditingController controldetalle = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        controller: controller,
        context: context,
        controluser: controluser,
      ),
      body: getInfo(context, controlp.readProfile(), controluser.uid),
    );
  }
}

@override
Widget getInfo(BuildContext context, Stream<QuerySnapshot> ct, String uid) {
  return StreamBuilder(
    stream: ct,
    /*FirebaseFirestore.instance
        .collection('clientes')
        .snapshots(),*/ //En esta línea colocamos el el objeto Future que estará esperando una respuesta
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
      print(snapshot.connectionState);
      switch (snapshot.connectionState) {

        //En este case estamos a la espera de la respuesta, mientras tanto mostraremos el loader
        case ConnectionState.waiting:
          return Center(child: CircularProgressIndicator());

        case ConnectionState.active:
          if (snapshot.hasError) return Text('Error: ${snapshot.error}');
          // print(snapshot.data);
          return snapshot.data != null
              ? VistaPerfil(perfil: snapshot.data!.docs, uid: uid)
              : Text('Sin Datos');

        /*
             Text(
              snapshot.data != null ?'ID: ${snapshot.data['id']}\nTitle: ${snapshot.data['title']}' : 'Vuelve a intentar',
              style: TextStyle(color: Colors.black, fontSize: 20),);
            */

        default:
          return Text('Presiona el boton para recargar');
      }
    },
  );
}

class VistaPerfil extends StatelessWidget {
  final List perfil;
  final String uid;

  const VistaPerfil({required this.perfil, required this.uid});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: perfil.length == 0 ? 0 : perfil.length,
        itemBuilder: (context, posicion) {
          print(perfil[posicion].id);
          return (uid == perfil[posicion]['uid'])
              ? Card(
                  elevation: 2,
                  child: Container(
                    padding: const EdgeInsets.only(
                        top: 4.0, bottom: 16.0, left: 8.0, right: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        // Row(
                        //   children: [
                        perfil[posicion]['fotoperfil'] != ''
                            ? ProfileWidget(
                                imagePath: perfil[posicion]['fotoperfil'],
                                onClicked: () {
                                  var profile = <String, dynamic>{
                                    'descripcion': perfil[posicion]
                                        ['descripcion'],
                                    'email': perfil[posicion]['email'],
                                    'fotoperfil': perfil[posicion]
                                        ['fotoperfil'],
                                    'nombre': perfil[posicion]['nombre'],
                                    'uid': perfil[posicion]['uid'],
                                  };
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => EditProfilePage(
                                              perfil: profile,
                                              uid: uid,
                                            )),
                                  );
                                },
                              )
                            : Icon(
                                Icons.camera_alt,
                                color: controller.darkMode
                                    ? Colors.white
                                    : Colors.grey[800],
                              ),

                        const SizedBox(height: 24),
                        buildName(perfil[posicion]['nombre'],
                            perfil[posicion]['email']),
                        const SizedBox(height: 24),
                        Center(child: buildUpgradeButton(context)),
                        const SizedBox(height: 24),
                        NumbersWidget(),
                        const SizedBox(height: 48),
                        buildAbout(perfil[posicion]['descripcion']),
                      ],
                      // ),
                    ),
                  ))
              : Text("Perfil No encontrado");
        });

  }

  Widget buildName(String nombre, String email) => Column(
        children: [
          Text(
            nombre,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            email,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildUpgradeButton(BuildContext context) => ButtonWidget(
    text: 'Hora de un retoque!',
    onClicked:  () async {
      await availableCameras().then(
            (value) => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CameraPage(cameras: value,),
          ),
        ),
      );
    },
  );

  Widget buildAbout(String descripcion) => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'About',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Text(
              descripcion,
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      );
}
