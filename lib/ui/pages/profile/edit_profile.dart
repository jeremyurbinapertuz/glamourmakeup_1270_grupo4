// import 'dart:io';
//
// import 'package:animated_theme_switcher/animated_theme_switcher.dart';
// import 'package:flutter/material.dart';
// import 'package:path/path.dart';
// import 'package:user_profile_ii_example/model/user.dart';
// import 'package:user_profile_ii_example/utils/user_preferences.dart';
// import 'package:user_profile_ii_example/widget/appbar_widget.dart';
// import 'package:user_profile_ii_example/widget/button_widget.dart';
// import 'package:user_profile_ii_example/widget/profile_widget.dart';
// import 'package:user_profile_ii_example/widget/textfield_widget.dart';
//
// class EditProfilePage extends StatefulWidget {
//   @override
//   _EditProfilePageState createState() => _EditProfilePageState();
// }
//
// class _EditProfilePageState extends State<EditProfilePage> {
//   User user = UserPreferences.myUser;
//
//   @override
//   Widget build(BuildContext context) => ThemeSwitchingArea(
//     child: Builder(
//       builder: (context) => Scaffold(
//         appBar: buildAppBar(context),
//         body: ListView(
//           padding: EdgeInsets.symmetric(horizontal: 32),
//           physics: BouncingScrollPhysics(),
//           children: [
//             ProfileWidget(
//               imagePath: user.imagePath,
//               isEdit: true,
//               onClicked: () async {},
//             ),
//             const SizedBox(height: 24),
//             TextFieldWidget(
//               label: 'Full Name',
//               text: user.name,
//               onChanged: (name) {},
//             ),
//             const SizedBox(height: 24),
//             TextFieldWidget(
//               label: 'Email',
//               text: user.email,
//               onChanged: (email) {},
//             ),
//             const SizedBox(height: 24),
//             TextFieldWidget(
//               label: 'About',
//               text: user.about,
//               maxLines: 5,
//               onChanged: (about) {},
//             ),
//           ],
//         ),
//       ),
//     ),
//   );
// }


import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:getglam/domain/use_case/controllers/controllerfirestore.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';
import 'package:getglam/domain/use_case/controllers/themecontroller.dart';
import 'package:image_picker/image_picker.dart';

class EditProfilePage extends StatefulWidget {
  final uid;
  final Map<String, dynamic> perfil;
  EditProfilePage({required this.perfil, this.uid,});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController controlnombre = TextEditingController();
  TextEditingController controldesc = TextEditingController();
  ControllerFirestore controlestados = Get.find();
  Controllerauth controluser = Get.find();
  PerfilController controlperfil = Get.find();

  late final ThemeController controller = Get.put(ThemeController());
  ImagePicker picker = ImagePicker();
  var _image;

  @override
  void initState() {
    controlnombre =
        TextEditingController(text: widget.perfil['nombre']);
    controldesc =
        TextEditingController(text: widget.perfil['descripcion']);

    // TODO: implement initState
    super.initState();
  }

  _galeria() async {
    XFile? image =
    await picker.pickImage(source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _image = (image != null) ? File(image.path) : null;
      //_image = File(image!.path);
    });
  }

  _camara() async {
    XFile? image =
    await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _image = (image != null) ? File(image.path) : null;
      // _image = File(image!.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Editar Perfil"),
        backgroundColor: Colors.purple.shade900,
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: ListView(
            children: <Widget>[
              Html(data:"<strong>Imagen</strong>:"),
              Center(
                child: GestureDetector(
                  onTap: () {
                    _opcioncamara(context);
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    height: 220,
                    width: double.maxFinite,
                    child: Card(
                      elevation: 5,
                      child: _image != null
                          ? Image.file(
                        _image,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      )
                          : Icon(
                        Icons.camera_alt,
                        color: controller.darkMode
                            ? Colors.white
                            : Colors.grey[800],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextField(
                controller: controlnombre,
                decoration: InputDecoration(labelText: "Nombre"),
              ),
              SizedBox(
                height: 15,
              ),
              TextField(
                controller: controldesc,
                decoration: InputDecoration(labelText: "Descripcion"),
              ),
              ElevatedButton(
                child: Text("Editar Perfil"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.purple.shade900, // background
                  onPrimary: Colors.white,
                ),
                onPressed: () {
                  var perfil = <String, dynamic>{
                    'nombre': controlnombre.text,
                    'descripcion': controldesc.text,
                    'uid': controluser.uid,
                  };

                  controlperfil.actualizarPerfil(controluser.getUid(), perfil, _image);
                  Get.back();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

//Seleccionar la camara o la galeria

  void _opcioncamara(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Imagen de Galeria'),
                      onTap: () {
                        _galeria();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Capturar Imagen'),
                    onTap: () {
                      _camara();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
