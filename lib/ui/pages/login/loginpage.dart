import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getglam/domain/use_case/controllers/connectivity.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController usuario = TextEditingController();
  TextEditingController passwd = TextEditingController();

  Controllerauth controluser = Get.find();

  late ConnectivityController connectivityController;

  @override
  void initState() {
    super.initState();
    autoLogIn();
    connectivityController = Get.find<ConnectivityController>();
  }


  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? email = prefs.getString('email');
    final String? passw = prefs.getString('pass');
    // print("email =>" + email!);
    // print("passw =>" + passw!);

    if (email != null) {
      setState(() {
        usuario.text = email;
        passwd.text = passw!;
        if (controluser.userf != 'Sin Registro')
          _iniLogin(usuario.text, passwd.text);
      });
      return;
    }
  }

  _registroylogin(theEmail, thePassword) async {
    print('_login $theEmail $thePassword');
    try {
      await controluser.registrarEmail(theEmail, thePassword);
      print(controluser.userf);
      if (controluser.userf != 'Sin Registro' && controluser.userf==theEmail) {
        // Get.offNamed('/content');
        return Future.delayed(Duration(milliseconds: 1500), () {
          Get.snackbar('${theEmail}', 'Wow, que bien te ves!');
          Get.toNamed("DASHBOARD");
        });
      } else {
        Get.snackbar(
          "Login",
          'Por Favor Ingrese un Email Valido',
          icon: Icon(Icons.person, color: Colors.red),
          snackPosition: SnackPosition.TOP,
        );
      }
    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  _iniLogin(theEmail, thePassword) async {
    print('_login $theEmail $thePassword');
    try {
      await controluser.signInWithEmailAndPassword(theEmail, thePassword);
      print(controluser.userf);
      if (controluser.userf != 'Sin Registro' && controluser.userf==theEmail) {
        // Get.offNamed('/content');
        return Future.delayed(Duration(milliseconds: 1500), () {
          Get.snackbar('${theEmail}', 'Wow, que bien te ves!');
          Get.toNamed("DASHBOARD");
        });
      } else {
        Get.snackbar(
          "Login",
          'Por Favor Ingrese un Email Valido',
          icon: Icon(Icons.person, color: Colors.red),
          snackPosition: SnackPosition.TOP,
        );
      }
    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  _google() async {
    try {
      await controluser.ingresarGoogle();
      //Get.to(() => ListaMensajes());

    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/img/makeup.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/img/glamlogp.png',
                  width: 100,
                  height: 170,
                  fit: BoxFit.cover,
                ),


                SizedBox(
                  height: 20,
                ),
                TextField(
                    key: const Key('userTextField'),
                    controller: usuario,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Ingrese correo',
                      hintStyle: new TextStyle(color: Colors.white),
                      labelStyle: new TextStyle(color: const Color(0xFF424242)),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.deepPurple.shade900),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.amber),
                      ),
                      border: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.deepPurple.shade900),
                      ),
                    )),
                TextField(
                    key: const Key('passTextField'),
                    controller: passwd,
                    style: TextStyle(color: Colors.white),
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: 'Ingrese clave',
                      hintStyle: new TextStyle(color: Colors.white),
                      border: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.deepPurple.shade800),
                      ),
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                        key: Key("siButt"),
                        color: Colors.white,
                        onPressed: () {
                          if (connectivityController.connected) {
                            _iniLogin(usuario.text, passwd.text);
                          } else {
                            Get.snackbar(
                              "Red",
                              'No hay Conexion de Red',
                              icon: Icon(Icons.wifi_off, color: Colors.blue),
                              snackPosition: SnackPosition.BOTTOM,
                            );
                          }
                        },
                        icon: Icon(Icons.login_outlined)),
                    IconButton(
                        key: Key("regButt"),
                        color: Colors.white,
                        onPressed: () {
                          if (connectivityController.connected) {
                            _registroylogin(usuario.text, passwd.text);
                          } else {
                            Get.snackbar(
                              "Red",
                              'No hay Conexion de Red',
                              icon: Icon(Icons.wifi_off, color: Colors.blue),
                              snackPosition: SnackPosition.BOTTOM,
                            );
                          }
                        },
                        icon: Icon(Icons.app_registration_outlined)),
                  ],
                ),
                // Obx(() => Text(controluser.userf)),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: const Key('sendGoogle'),
        onPressed: () {
          if (connectivityController.connected) {
            _google();
          } else {
            Get.snackbar(
              "Red",
              'No hay Conexion de Red',
              icon: Icon(Icons.wifi_off, color: Colors.blue),
              snackPosition: SnackPosition.BOTTOM,
            );
          }
        },
        child: FaIcon(
          FontAwesomeIcons.google,
          color: Colors.white,
        ),
        backgroundColor: Colors.deepPurple.shade900,
        //Icon(Icons.add),
      ),
    );
  }
}
