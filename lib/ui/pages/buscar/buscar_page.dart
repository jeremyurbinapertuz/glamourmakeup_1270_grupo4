import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/buscar_controller.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';
import 'package:getglam/domain/use_case/controllers/themecontroller.dart';
import 'package:getglam/ui/pages/response/screens/response_screen.dart';
import 'package:getglam/ui/widgets/appbar.dart';

class BuscarPage extends GetView<BuscarController> {

  final Controllerauth controluser = Get.find();



  @override
  Widget build(BuildContext context) {
    final ThemeController controller = Get.find();
    return Scaffold(
      appBar: CustomAppBar(
        controller: controller,
        controluser: controluser,
        context: context,
      ),

      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
          child: ResponseScreen(controller: controller),
        ),
      ),
    );
  }
}
