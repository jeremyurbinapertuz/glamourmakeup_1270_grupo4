import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class LocationController extends GetxController {

  static final FirebaseFirestore _db = FirebaseFirestore.instance;
  late Rx<dynamic> _posicionlat = "".obs;
  late Rx<dynamic> _posicionlo = "".obs;

  late Rx<dynamic> _cercanos = "0".obs;

  String get locationlat => _posicionlat.value;
  String get locationlo => _posicionlo.value;
  String get cercanos => _cercanos.value;

  Future<void> obtenerubicacion() async {
    late LocationData _posicion;
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    print('Posicion 000');

    _serviceEnabled = await location.serviceEnabled();
    print('Posicion 001');
    if (!_serviceEnabled) {
      print('Posicion 002');
      _serviceEnabled = await location.requestService();
      print('Posicion 003');
      if (!_serviceEnabled) {
        print('Posicion 004');
        return;
      }
    }

    print('Posicion 005');
    _permissionGranted = await location.hasPermission();
    print('Posicion 006');

    if (_permissionGranted == PermissionStatus.denied) {
      print('Posicion 007');
      _permissionGranted = await location.requestPermission();
      print('Posicion 008');
      if (_permissionGranted != PermissionStatus.granted) {
        print('Posicion 009');
        return;
      }
    }

    print('Posicion 010');
    _posicion = await location.getLocation();
    print('Posicion 011');

    print('Posicion:-> ' + _posicion.toString());
    _posicionlat.value = _posicion.latitude.toString();
    _posicionlo.value = _posicion.longitude.toString();
  }

  set cercanos(String cercanos) {
    _cercanos.value = cercanos;
  }


  Stream<QuerySnapshot> readLocations() {
    CollectionReference listado = _db.collection('ubicacion');
    print("listado.snapshots() => " + listado.snapshots().toString());
    return listado.snapshots();
  }

  Future<void> guardarubicacion(Map<String, dynamic> ubicacion, uid) async {
    await _db.collection('ubicacion').doc(uid).set(ubicacion).catchError((e) {
      print(e);
    });
    //return true;
  }

}
