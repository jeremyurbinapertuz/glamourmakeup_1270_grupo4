import 'dart:async';

import 'package:getglam/domain/models/mensajes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';


class ChatController extends GetxController {



  final DatabaseReference _mensajesRef =
  FirebaseDatabase.instance.reference().child('mensajes');




  Future<void> guardarMensaje(Mensaje mensaje) async {
    String uid = FirebaseAuth.instance.currentUser!.uid;
    try {
      _mensajesRef.push().set(mensaje.toJson());
    } catch(e){
      Get.snackbar('algo salio muy mal', e.toString(),
          snackPosition: SnackPosition.BOTTOM);
      return Future.error(e);
    }
  }

  void deleteMensaje(String idmensaje) {
    _mensajesRef.child(idmensaje).remove();
  }

  Query getMensajes() => _mensajesRef;


}