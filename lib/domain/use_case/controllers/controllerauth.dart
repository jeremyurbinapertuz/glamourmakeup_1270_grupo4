import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:getglam/domain/use_case/controllers/perfil_controller.dart';
import 'package:getglam/ui/pages/login/loginpage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class Controllerauth extends GetxController {
  final FirebaseAuth auth = FirebaseAuth.instance;

  static final FirebaseFirestore _db = FirebaseFirestore.instance;
  late Rx<dynamic> _usuarior = "Sin registro".obs;
  late Rx<dynamic> _uid = "".obs;
  late Rx<dynamic> _name = "".obs;
  late Rx<dynamic> _photo = "".obs;

  String get userf => _usuarior.value;
  String get uid => _uid.value;

  String get name => _name.value;
  String get photo => _photo.value;

  late final PerfilController controlperfil = Get.put(PerfilController());

  Future<void> registrarEmail(dynamic _email, dynamic _passw) async {
    try {
      UserCredential usuario = await auth.createUserWithEmailAndPassword(
          email: _email, password: _passw);

      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;
      _name.value = usuario.user!.email;
      print("usuario => " + usuario.toString());

      print("no tiene perfil");
      var perfil = <String, dynamic>{
        'descripcion': '',
        'email': _usuarior.value,
        'fotoperfil': '',
        'nombre': _name.value,
        'uid': _uid.value,
      };
      print("crear perfil");
      controlperfil.crearPerfil(_uid.value, perfil);

      // Inicializar las variables del perfil
      controlperfil.getProfileUid();

      // Get.snackbar('${usuario.user!.email}', 'Wow, que bien te ves!');
      //
      // return Future.delayed(Duration(milliseconds: 1500), () {
      //   Get.toNamed("DASHBOARD");
      // });
      return Future.value(true);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print("The password provided is too weak.");
        return Future.error('La contraseña es muy débil');
      } else if (e.code == 'email-already-in-use') {
        print('Correo ya Existe');
        return Future.error(
            'Ya existe una cuenta asociada a este correo');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> signInWithEmailAndPassword(dynamic email, dynamic passw) async {
    try {
      UserCredential usuario =
          await auth.signInWithEmailAndPassword(email: email, password: passw);
      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;
      _name.value = usuario.user!.email;

      DocumentReference<Map<String, dynamic>> snap =
          await _db.collection('perfil').doc(_uid.value);
      if (snap.id != _uid.value) {
        print("no tiene perfil");
        var perfil = <String, dynamic>{
          'descripcion': '',
          'email': _usuarior.value,
          'fotoperfil': '',
          'nombre': _name.value,
          'uid': _uid.value,
        };
        print("crear perfil");
        controlperfil.crearPerfil(_uid.value, perfil);
      } else {
        print("ya tiene perfil");
      }

      // Inicializar las variables del perfil
      controlperfil.getProfileUid();

      // Get.snackbar('${usuario.user!.email}', 'Wow, que bien te ves!');
      //
      // return Future.delayed(Duration(milliseconds: 1500), () {
      //   Get.toNamed("DASHBOARD");
      // });
      return Future.value(true);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('Correo no encontrado');
        return Future.error('Correo no encontrado');
      } else if (e.code == 'wrong-password') {
        print('Contraseña incorrecta');
        return Future.error('Contraseña incorrecta');
      }
    }
  }

  Future<void> ingresarGoogle() async {
    // Trigger the authentication flow

    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      // Obtain the auth details from the request

      final GoogleSignInAuthentication googleAuth =
          await googleUser!.authentication;

      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      UserCredential usuario =
          await FirebaseAuth.instance.signInWithCredential(credential);
      // Once signed in, return the UserCredential

      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;

      _name.value = usuario.user!.displayName;
      _photo.value = usuario.user!.photoURL;

      DocumentReference<Map<String, dynamic>> snap =
          await _db.collection('perfil').doc(_uid.value);
      if (snap.id != _uid.value) {
        print("no tiene perfil");
        var perfil = <String, dynamic>{
          'descripcion': '',
          'email': _usuarior.value,
          'fotoperfil': _photo.value,
          'nombre': _name.value,
          'uid': _uid.value,
        };
        print("crear perfil");
        controlperfil.crearPerfil(_uid.value, perfil);
      } else {
        print("ya tiene perfil");
      }

      // Inicializar las variables del perfil
      controlperfil.getProfileUid();

      Get.snackbar('${usuario.user!.email}', 'Wow, que bien te ves!');

      return Future.delayed(Duration(milliseconds: 1500), () {
        Get.toNamed("DASHBOARD");
      });
    } catch (e) {
      return Future.error('Error');
    }
  }

  Future<void> signOut() async {
    try {
      await auth.signOut();
      Get.to(() => Login());
      Get.snackbar('Sesión Cerrada', 'Nos vemos luego!', colorText: Color.fromRGBO(255, 255, 255, 1));
    } on FirebaseAuthException catch (e) {
      Get.snackbar('algo salio muy mal', e.toString(),
          snackPosition: SnackPosition.BOTTOM);
    }
  }

  String getUid() {
    String uid = FirebaseAuth.instance.currentUser!.uid;
    return uid;
  }
}
