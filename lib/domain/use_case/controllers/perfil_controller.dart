import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:firebase_storage/firebase_storage.dart' as fs;
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:getglam/domain/use_case/controllers/controllerauth.dart';


class PerfilController extends GetxController {
  static final fs.FirebaseStorage storage = fs.FirebaseStorage.instance;
  static final FirebaseFirestore _db = FirebaseFirestore.instance;
  Controllerauth controluser = Get.find();
  late Rx<dynamic> _name = "".obs;
  late Rx<dynamic> _photo = "".obs;
  late Rx<dynamic> _desc = "".obs;
  late Rx<dynamic> _uid = "".obs;

  String get nombre => _name.value;
  String get fotoperfil => _photo.value;
  String get descripcion => _desc.value;
  String get uid => _uid.value;


  Stream<QuerySnapshot> readProfile() {
    String uid = controluser.getUid();
    CollectionReference listado = _db.collection('perfil');
    return listado.where('uid', isEqualTo: uid).snapshots();
  }


  String getProfileUid() {
    getNombre();
    getFoto();
    getDescripcion();
    String uid = controluser.getUid();
    var docRef = _db.collection("perfil").doc(uid);
    var profileUid = "";
    docRef.get().then((doc) => {
      if (doc.exists) {
        profileUid = doc.data()!['uid'],
        _uid.value = profileUid,
        print("_uid.value:" + _uid.value),
      } else {
        profileUid = ""
      }
    });
    return profileUid;
  }

  String getNombre() {
    String uid = controluser.getUid();
    var docRef = _db.collection("perfil").doc(uid);
    String nombre = "";
    docRef.get().then((doc) => {
      if (doc.exists) {
        nombre = doc.data()!['nombre'],
        _name.value = nombre,
        print("_name.value:" + _name.value),
      } else {
        nombre = ""
      }
    });
    return nombre;
  }

  String getFoto() {
    String uid = controluser.getUid();
    var docRef = _db.collection("perfil").doc(uid);
    String foto = "";
    docRef.get().then((doc) => {
      if (doc.exists) {
        foto = doc.data()!['fotoperfil'],
        _photo.value = foto,
        print("_photo.value:" + _photo.value),
      } else {
        foto = ""
      }
    });
    return foto;
  }

  String getDescripcion() {
    String uid = controluser.getUid();
    var docRef = _db.collection("perfil").doc(uid);
    String desc = "";
    docRef.get().then((doc) => {
      if (doc.exists) {
        desc = doc.data()!['descripcion'],
        _desc.value = desc,
        print("_desc.value:" + _desc.value),
      } else {
        desc = ""
      }
    });
    return desc;
  }

  Future<void> crearPerfil(String id, Map<String, dynamic> perfil) async {
    await _db.collection('perfil').doc(id).set(perfil).catchError((e) {
      print(e);
    });
    //return true;
  }


  Future<void> actualizarPerfil(String id, Map<String, dynamic> perfil, foto) async {
    var url = '';
    // DateTime.now().toString();
    if (foto != null){
      url = await cargarfoto(foto, id);
      print(url);
      perfil['fotoperfil'] = url.toString();
    }

    await _db.collection('perfil').doc(id).update(perfil).catchError((e) {
      print(e);
    });

    Get.snackbar(
        getNombre(),
        'Perfil actualizado correctamente!'
    );
    //return true;
  }

  Future<dynamic> cargarfoto(var foto, var idfoto) async {
    final fs.Reference storageReference =
    fs.FirebaseStorage.instance.ref().child("Perfil");

    fs.TaskSnapshot taskSnapshot =
    await storageReference.child(idfoto).putFile(foto);

    var url = await taskSnapshot.ref.getDownloadURL();
    print('url:' + url.toString());
    return url.toString();
  }

}